#!/bin/sh

echo "Setting up your Mac..."

# Check for Oh My Zsh Installation
if test ! $(which omz); then
  /bin/sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/HEAD/tools/install.sh)"
fi

# Check for Homebrew Installation
if test ! $(which brew); then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> $HOME/.zprofile
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Update Homebrew recipes
brew update

# Install all our dependencies with bundle (See Brewfile)
brew tap homebrew/bundle
brew bundle --file $DOTFILES/Brewfile

# Create a Projects directory
mkdir $HOME/Projects

# Clone Github repositories
$DOTFILES/clone.sh

# Stow dotfiles into ~/
stow .

# Set macOS preferences - we will run this last because this will reload the shell
source $DOTFILES/scripts/.macos

echo "Setup done ✅"
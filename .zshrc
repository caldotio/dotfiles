# .zshrc config file

# Loading our .dotfiles (aliases, functions, exports)

for file in ~/.dotfiles/.{aliases,functions,exports}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Set name of the theme to load
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(
    git
    dotenv
    macos
    last-working-dir
    zsh-history-substring-search
    zsh-completions
    zsh-autosuggestions
    z
)

# init pyenv
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

eval $(thefuck --alias)

# prezto
source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"

# zsh-completions
autoload -U compinit && compinit

source $ZSH/oh-my-zsh.sh